class TestBugJ1 {
    public static void main(String[] a) {
	System.out.println(new Test().f());
    }
}

class Test {
	public int f(){
		boolean x = true;
		return x += 1;	// Should cause a good compiler to fail
	}
}

