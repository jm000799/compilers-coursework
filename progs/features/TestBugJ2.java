class TestBugJ2 {
    public static void main(String[] a) {
	System.out.println(new Test().f());
    }
}

class Test {
    public int f(){
        int x = 1;
        int y = 2;

        x += y;

        if(x == y) { // Checks if x has been set to y
            System.out.println("Error: X has been set to Y instead of incremented by Y");
        }

        return y;
    }
}

