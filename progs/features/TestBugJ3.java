class TestBugJ3 {
    public static void main(String[] a) {
	System.out.println(new Test().f());
    }
}

class Test {
    public int f(){
        int x = 1;
        int y = 2;

        x += y;

        if(x == 1) { // Checks if x remains the same
            System.out.println("Error: X has not been incremented by Y");
        }

        return x;
    }
}

